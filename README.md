# LingLongGUI

## 介绍
玲珑GUI是高效的界面开发解决方案。  
代替串口屏、组态，降低产品成本，产品软硬件自主可控。  
配套界面开发软件，图形化编辑界面，生成C代码，直接和用户产品代码结合。  
配套下载升级软件和bootloader，解决产品升级功能和图片下载问题。  

## 目的
提高开发效率，降低开发难度，降低产品成本

## 控件列表

| 状态 | 名称 | 说明 |
| :----:| ---- | ---- |
| ✅ | window | 窗体，多用于分层 |
| ✅ | button | 按键 |
| ✅ | image | 图片，用工具生成图片数据，支持png格式 |
| ✅ | text | 文本 |
| ✅ | check box | 复选框 |
| ✅ | line edit | 编辑框 |
| ✅ | slider | 滑动条 |
| ✅ | progress bar | 进度条 |
| ✅ | combo box | 下拉框 |
| ✅ | timer | 软件定时器 |
| ✅ | qr code | 二维码 |
| ✅ | gauge | 仪表盘 |
| ✅ | date time | 日期和时间 |
| ✅ | icon slider | 滑动图标 |

## 例子
examples文件夹中，有PC模拟运行的例子

可以使用qt+sdl或vscode+sdl的方式进行模拟运行

📖 [sdl例子使用教程](./examples/sdl/README.md)

## 配置模板

template文件夹中，有配置文件模板

## 教程
详细教程请查看  
📖 [玲珑GUI教程](https://www.yuque.com/books/share/3317aaa7-f47f-4bfd-a4c2-7e64e7f1c4be)  
📖 [玲珑swm32s Nano](https://www.yuque.com/books/share/b65c977d-f939-4888-9c8b-2d966053073c)  

🛠️ [llgui配套上位机](./tools/)  

|ℹ️ Note|
|:----|
|安装上位机后，务必手动更新安装目录下的LingLongGUI文件夹源码。插入keil中的文件会直接复制该文件夹内源码。|

|ℹ️ Note 实体按键支持|
|:----|
|如需要实体按键处理，请移步至ldgui复制xBtnAction，可实现多功能按键<br>🏠️主仓库: https://gitee.com/gzbkey/LingDongGUI<br>🏠️镜像仓库: https://github.com/gzbkey/LingDongGUI|


## 技术讨论
优先加新群  
QQ群1：1004783094  
QQ群2：473465075  

## 版权声明
本软件协议使用Apache License, Version 2.0  
附加协议：  
如果使用本软件编写二次开发类产品，包括且不限于串口屏、组态屏、二次开发屏幕模组，  
二次开发类产品则需要使用符合GPL兼容自由软件许可证或获得商业授权  

简而言之，允许开发终端类产品，禁止未授权的闭源二次开发类产品  
  
GPL兼容自由软件许可证种类参考  
http://www.gnu.org/licenses/license-list.en.html





