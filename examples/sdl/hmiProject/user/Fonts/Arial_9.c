#include "Arial_9.h"

const static llLatticeInfo Arial_9[]={
    {0x00,0x42,8,15,0,0}, /* B */
    {0x00,0x75,7,15,0,60}, /* u */
    {0x00,0x74,3,15,0,113}, /* t */
    {0x00,0x6f,7,15,0,136}, /* o */
    {0x00,0x6e,7,15,0,189}, /* n */
};

llLatticeLibraryInfo Arial_9_Lib={
    Arial_9,4,5,9
};
