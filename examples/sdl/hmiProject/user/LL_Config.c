/*
 * Copyright 2021-2024 Ou Jianbo 59935554@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * additional license
 * If you use this software to write secondary development type products,
 * must be released under GPL compatible free software license or commercial
 * license.
*/

#include "LL_Config.h"
#include "string.h"
#include "LL_User.h"
#include "freeRtosHeap4.h"
#include <time.h>

uint8_t cfgColorDepth = CONFIG_COLOR_DEPTH;
uint16_t cfgMonitorWidth = 0;
uint16_t cfgMonitorHeight = 0;

#if USE_USER_MEM == 1
void *llMalloc(uint32_t size)
{
    return pvPortMalloc(size);
}

void llFree(void *p)
{
    vPortFree(p);
    p=NULL;
}

void *llRealloc(void *ptr,uint32_t newSize)
{
    void *new_addr=NULL;
    if(ptr)
    {
        if(newSize!=0)
        {
            new_addr=llMalloc(newSize);

            if(new_addr!=NULL)
            {
                memcpy(new_addr,ptr,newSize);
                llFree(ptr);
            }
        }
    }
    else
    {
        if(newSize!=0)
        {
            new_addr=llMalloc(newSize);
        }
    }
    return new_addr;
}
#endif

void llCfgDoubleBufferStart(bool isCopy)
{
}

void llCfgDoubleBufferEnd(bool isRefreshNow)
{
}

/***************************************************************************//**
 * @fn         bool llCfgClickGetPoint(int16_t *x,int16_t *y)
 * @brief      获取触摸坐标
 * @param      *x,*y  触摸坐标
 * @return     bool 是否有触摸
 * @version    V0.1
 * @date       
 * @details    
 ******************************************************************************/
bool llCfgClickGetPoint(int16_t *x,int16_t *y)
{
    bool touchState=false;
    int16_t rx;
    int16_t ry;

    touchState=vtMouseGetPoint(&rx,&ry);
    *x=rx;
    *y=ry;
    if((touchState!=0)&&(((rx!=-1)&&(ry!=-1))||((rx!=0)&&(ry!=0))))
    {
        touchState=true;
    }
    else
    {
        touchState=false;
        *x=-1;
        *y=-1;
    }
    return touchState;
}

void llCfgSetPoint(int16_t x,int16_t y,llColor color)
{
    vtSetPoint(x,y,color);
}

#if USE_USER_FILL_COLOR == 1
/***************************************************************************//**
 * @fn         void llCfgFillSingleColor(int16_t x0,int16_t y0,int16_t x1,int16_t y1,llColor *color)
 * @brief      单色填充
 * @param      x0,y0,x1,y1  填充单色矩形的4个点
 * @return     void
 * @version    V0.1
 * @date       
 * @details    
 ******************************************************************************/
void llCfgFillSingleColor(int16_t x0,int16_t y0,int16_t x1,int16_t y1,llColor color)
{
    vtFillSingleColor(x0,y0,x1,y1,color);
}
#endif

#if USE_USER_FILL_MULTIPLE_COLORS == 1
/***************************************************************************//**
 * @fn         void llCfgFillMultipleColors(int16_t x0,int16_t y0,int16_t x1,int16_t y1,llColor *color)
 * @brief      彩色填充
 * @param      x0,y0,x1,y1  填充彩色矩形的4个点
 * @return     void
 * @version    V0.1
 * @date       
 * @details    
 ******************************************************************************/
void llCfgFillMultipleColors(int16_t x0,int16_t y0,int16_t x1,int16_t y1,llColor *color)
{
    vtFillMultipleColors(x0,y0,x1,y1,color);
}
#endif

/***************************************************************************//**
 * @fn         void llExFlashInit(void)
 * @brief      配合读数据前的初始化
 * @param      
 * @return     void
 * @version    V0.1
 * @date       
 * @details    
 ******************************************************************************/
void llExFlashInit(void)
{
}

/***************************************************************************//**
 * @fn         void llReadExFlash(uint32_t addr,uint8_t* pBuffer,uint16_t length)
 * @brief      读外部flash数据
 * @param      addr    地址
 *             pBuffer 数据缓存指针
 *             length  读取数据的长度
 * @return     void
 * @version    V0.1
 * @date       
 * @details    通常是读外部norflash,可以用数组代替,也可以是tf等
 ******************************************************************************/
void llReadExFlash(uint32_t addr,uint8_t* pBuffer,uint16_t length)
{
    norRead(pBuffer,addr,length);
}

/***************************************************************************//**
 * @fn         void llBuzzerBeep(void)
 * @brief      蜂鸣器触发响一声
 * @param      
 * @return     void
 * @version    V0.1
 * @date       
 * @details    
 ******************************************************************************/
void llBuzzerBeep(void)
{
}

/***************************************************************************//**
 * @fn         void llGetRtc(uint8_t *readBuf)
 * @brief      读取年月日时分秒周
 * @param      *readBuf yy yy mm dd hh mm ss ww
 * @return     void
 * @version    V0.1
 * @date       
 * @details    数据用16进制储存,2021年 yyyy=0x07E5
 ******************************************************************************/
void llGetRtc(uint8_t *readBuf)
{
    time_t t;
    struct tm * lt;
    
    time (&t);
    lt = localtime (&t);

    readBuf[0]=((lt->tm_year+1900)>>8)&0xFF;
    readBuf[1]=(lt->tm_year+1900)&0xFF;
    readBuf[2]=(lt->tm_mon+1)&0xFF;
    readBuf[3]=lt->tm_mday&0xFF;
    readBuf[4]=lt->tm_hour&0xFF;
    readBuf[5]=lt->tm_min&0xFF;
    readBuf[6]=lt->tm_sec&0xFF;
    readBuf[7]=lt->tm_wday&0xFF;
}

/***************************************************************************//**
 * @fn         void llSetRtc(uint8_t *writeBuf)
 * @brief      写入年月日时分秒
 * @param      *writeBuf yy yy mm dd hh mm ss
 * @return     void
 * @version    V0.1
 * @date       
 * @details    数据用16进制储存,2021年 yyyy=0x07E5
 ******************************************************************************/
void llSetRtc(uint8_t *writeBuf)
{
}

