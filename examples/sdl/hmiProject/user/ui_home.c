#include "ui_home.h"
#include "ui_homeLogic.h"
#include "LL_General.h"
#include "LL_Gui.h"
#include "LL_Linked_List.h"
#include "LL_Timer.h"
#include "LL_ButtonEx.h"
#ifdef USE_LLGUI_EX
#include "LL_GuiEx.h"
#endif


void ui_homeInit(void)
{
    llDoubleBufferStart(false);
    //背景
    llBackgroundQuickCreate(ID_BACKGROUND,LL_MONITOR_WIDTH,LL_MONITOR_HEIGHT,true,RGB888(0x9db1f1),0);

    //按键
    llButtonQuickCreate(ID_BUTTON_0,ID_BACKGROUND,40,30,80,30,(uint8_t *)"Button",FONT_LIB_C_ARIAL_9,RGB888(0x000000),RGB888(0x55aaff),RGB888(0xfff14e),0xFFFFFFFF,0xFFFFFFFF,BUTTON_DIS_TYPE_COLOR,false);
    nButtonSetEnabled(ID_BUTTON_0,true);
    nButtonSetCheckable(ID_BUTTON_0,false);
    nButtonSetKeyValue(ID_BUTTON_0,0);

    llImageCreate(ID_IMAGE_0,ID_BACKGROUND,90,110,IMAGE_ARC51_PNG_ADDR,false);
    nImageSetEnabled(ID_IMAGE_0,true);

    llDoubleBufferEnd(true);
    ui_homeLogicInit();
}

void ui_homeLoop(void)
{
    ui_homeLogicLoop();
}

void ui_homeQuit(void)
{
    ui_homeLogicQuit();
}

