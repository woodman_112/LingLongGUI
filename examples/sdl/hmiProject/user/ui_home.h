#ifndef _UI_HOME_H_
#define _UI_HOME_H_

#ifdef __cplusplus
extern "C" {
#endif

void ui_homeInit(void);

void ui_homeLoop(void);

void ui_homeQuit(void);

#ifdef __cplusplus
}
#endif

#endif //_UI_HOME_H_

