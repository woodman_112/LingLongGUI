TEMPLATE = app
#CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += $$files(./user/*.h) \
           $$files(./virtualNor/*.h) \
           $$files(../../Gui/*.h) \
           $$files(../../Misc/*.h) \
           $$files(hmiProject/user/*.h,true)

SOURCES += $$files(./user/*.c) \
           $$files(./virtualNor/*.c) \
           $$files(../../Gui/*.c) \
           $$files(../../Misc/*.c) \
           $$files(hmiProject/user/*.c,true)

INCLUDEPATH += ./user \
               ./virtualNor \
               ../../Gui \
               ../../Misc \
               hmiProject/user \
               hmiProject/user/Fonts

contains(QT_ARCH, i386){
INCLUDEPATH += $$PWD/sdl2/32/include/SDL2
LIBS += -L$$PWD/sdl2/32/lib -lSDL2
LIBS += -L$$PWD/sdl2/32/lib -lSDL2main
SDL2_PATH = $$PWD/sdl2/32/bin/SDL2.dll
}else{
INCLUDEPATH += $$PWD/sdl2/64/include/SDL2
LIBS += -L$$PWD/sdl2/64/lib -lSDL2
LIBS += -L$$PWD/sdl2/64/lib -lSDL2main
SDL2_PATH = $$PWD/sdl2/64/bin/SDL2.dll
}

SDL2_PATH = $$replace(SDL2_PATH, /, \\)
OutLibFile = $$OUT_PWD/SDL2.dll
OutLibFile = $$replace(OutLibFile, /, \\)
QMAKE_PRE_LINK +=  copy $$SDL2_PATH $$OutLibFile /y

