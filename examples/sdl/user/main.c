#include <stdio.h>
#include "SDL.h"
#undef main
#include "sdlPort.h"
#include "LL_Gui.h"
#include "LL_Timer.h"
#include "LL_Characters.h"
#include "LL_Background.h"

uint32_t vtTimerCallback(uint32_t interval, void *param)
{
    llGuiTick(10);
    return interval;
}

int main (void) 
{
    setbuf(stdout,NULL);

    printf("  _       _        _____  _    _  _____ \n"
           " | |     | |      / ____|| |  | ||_   _|\n"
           " | |     | |     | |  __ | |  | |  | |  \n"
           " | |     | |     | | |_ || |  | |  | |  \n"
           " | |____ | |____ | |__| || |__| | _| |_ \n"
           " |______||______| \\_____| \\____/ |_____|\n\n"
           );

    vtInit();
    norSetBin("./../sdl/hmiProject/user/image.bin");
	
    SDL_GetTicks();
    SDL_AddTimer(10, vtTimerCallback, NULL);

    llGuiInit();

    while (1)
    {
        llGuiLoop();
    }

}


