#ifndef _SDL_PORT_H_
#define _SDL_PORT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

//
// 虚拟屏幕设定参数,即需要一个什么样的屏幕
//
#define VT_WIDTH                 480
#define VT_HEIGHT                272
#define VT_COLOR_DEPTH           16
#define VT_VIRTUAL_MACHINE       0                   /*Different rendering should be used if running in a Virtual machine*/

#if VT_COLOR_DEPTH == 1 || VT_COLOR_DEPTH == 8 || VT_COLOR_DEPTH == 16 || VT_COLOR_DEPTH == 24 || VT_COLOR_DEPTH == 32
#if VT_COLOR_DEPTH == 1 || VT_COLOR_DEPTH == 8
typedef uint8_t color_typedef;
#elif VT_COLOR_DEPTH == 16
typedef uint16_t color_typedef;
#elif VT_COLOR_DEPTH == 24 || VT_COLOR_DEPTH == 32
typedef uint32_t color_typedef;
#endif
#else
#error "Invalid VT_COLOR_DEPTH in Virtual_TFT_Port.h"
#endif




void vtInit(void);
void vtFillSingleColor(int32_t x1, int32_t y1, int32_t x2, int32_t y2, color_typedef color);
void vtFillMultipleColors(int32_t x1, int32_t y1, int32_t x2, int32_t y2, color_typedef * color_p);
void vtSetPoint(int32_t x, int32_t y, color_typedef color);
color_typedef vtGetPoint(int32_t x, int32_t y);
bool vtMouseGetPoint(int16_t *x,int16_t *y);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //_SDL_PORT_H_
